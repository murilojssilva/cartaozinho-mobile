import React, { useState, useEffect } from "react";
import { Text, ScrollView, Picker } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { useNavigation } from "@react-navigation/native";
import logoImg from "../../assets/icon.png";
import api from "../../services/api";
import ibge from "../../services/ibge";

import {
  Container,
  Header,
  Logo,
  ProvidersList,
  ProvidersTitle,
  Filter,
  Form,
  Input,
  ProviderBox,
  List,
  Name,
  Description,
  ProviderLogo,
  DetailsButton,
  FilterInput,
} from "./styles";

export default function Services() {
  const [providers, setProviders] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [pickerVisible, setPickerVisible] = useState(false);
  const [error, setError] = useState(false);
  const [uf, setUf] = useState([]);
  const [city, setCity] = useState([]);
  const [ufSelect, setUfSelect] = useState("");
  const [citySelect, setCitySelect] = useState("");
  const [type, setType] = useState("");
  const navigation = useNavigation();
  function navigateToDetails(provider) {
    navigation.navigate("Details", { provider });
  }
  async function loadCity() {
    if (ufSelect !== "") {
      const response = await ibge.get(`estados/${ufSelect}/municipios`);
      setCity(response.data);
      setPickerVisible(true);
    }
  }
  async function loadUf() {
    const response = await ibge.get("estados");
    setUf(response.data);
    setLoading(false);
  }

  useEffect(() => {
    loadUf();
    async function loadProviders() {
      const response = await api.get("/providers");
      setProviders(response.data);
    }
    loadProviders();
  }, []);
  useEffect(() => {
    loadCity();
  }, [ufSelect]);
  return (
    <Container>
      <Header>
        <Logo source={logoImg} />
      </Header>
      <ProvidersList>
        <ScrollView
          vertical
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ paddingHorizontal: 20 }}
        >
          {loading ? (
            <Grid>
              <Text>Carregando...</Text>
            </Grid>
          ) : (
            <>
              <ProvidersTitle>Prestadores de serviço</ProvidersTitle>
              <Filter>
                <FilterInput
                  value={ufSelect}
                  error={error}
                  onChange={(e) => setUfSelect(e.target.value)}
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="UF"
                />
                <FilterInput
                  value={citySelect}
                  error={error}
                  onChange={(e) => setCitySelect(e.target.value)}
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Cidade"
                />
                <FilterInput
                  value={type}
                  error={error}
                  onChange={(e) => setType(e.target.value)}
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Tipo de pessoa"
                />
              </Filter>
              <Form>
                <Input
                  value={searchText}
                  error={error}
                  onChange={(e) => setSearchText(e.target.value)}
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Nome"
                />
              </Form>
              <List
                KeyboardShouldPersist
                data={providers
                  .filter((provider) =>
                    provider.name.toLowerCase().includes(searchText)
                  )
                  .filter((provider) =>
                    provider.uf.toLowerCase().includes(ufSelect)
                  )
                  .filter((provider) =>
                    provider.city.toLowerCase().includes(citySelect)
                  )
                  .filter((provider) =>
                    provider.type.toLowerCase().includes(type)
                  )
                  .sort((a, b) => a.name.localeCompare(b.name))}
                keyExtractor={(provider) => String(provider._id)}
                showsVerticalScrollIndicator={false}
                onEndReachedThreshold={0.2}
                renderItem={({ item: provider }) => (
                  <DetailsButton onPress={() => navigateToDetails(provider)}>
                    <ProviderBox>
                      <Name>{provider.name}</Name>
                      <ProviderLogo
                        source={`https://cartaozinho-backend.herokuapp.com/files/${provider.avatar}`}
                      />
                      <Description>{provider.description}</Description>
                    </ProviderBox>
                  </DetailsButton>
                )}
              />
            </>
          )}
        </ScrollView>
      </ProvidersList>
    </Container>
  );
}
function Grid({ children }) {
  return (
    <div className="grid">
      <LoadingBox>{children}</LoadingBox>
    </div>
  );
}

function LoadingBox({ children }) {
  return React.Children.map(children, (child) => {
    return <div className="loading-box">{child}</div>;
  });
}
