import styled from "styled-components/native";
import { LinearGradient } from "expo-linear-gradient";
import Constants from "expo-constants";

export const Container = styled(LinearGradient).attrs({
  colors: ["#F8F8FF", "#FFFAF0"],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  padding-top: ${Constants.statusBarHeight} + 20px;
`;

export const Logo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
`;
export const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 20px;
`;
export const ProvidersList = styled.View`
  flex: 1;
`;
export const ProvidersTitle = styled.Text`
  font-size: 32px;
  color: #008037;
  font-weight: bold;
  padding: 0 20px;
`;
export const Form = styled.View`
  flex-direction: row;
  margin: 10px 0;
  padding: 0 20px;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: "#008037",
})`
  flex: 1;
  padding: 12px 15px;
  border-radius: 4px;
  font-size: 16px;
  color: #333;
  background: #fff;
  border: 2px solid #008037;
`;
export const FilterInput = styled.TextInput.attrs({
  placeholderTextColor: "#008037",
})`
  padding: 12px 15px;
  border-radius: 4px;
  font-size: 16px;
  color: #333;
  background: #fff;
  border: 2px solid #008037;
  max-width: 100px;
  margin: 0 5px;
`;

export const Search = styled.TouchableOpacity`
  background: #008037;
  margin-left: 10px;
  justify-content: center;
  border-radius: 4px;
  padding: 0 14px;
`;

export const Filter = styled.View`
  display: flex;
  flex-direction: row;
  width: 90%;
  margin: 0 auto;
`;

export const List = styled.FlatList.attrs({
  contentContainerStyle: { paddingHorizontal: 20 },
  showsVerticalScrollIndicator: false,
})`
  margin-top: 20px;
`;
export const ProviderBox = styled.View`
  padding: 20px;
  border-radius: 4px;
  background-color: #008037;
  margin-bottom: 15px;
`;

export const Name = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: #fff;
`;

export const Description = styled.Text.attrs({
  numberOfLines: 2,
})`
  color: #fff;
  text-align: center;
  margin-top: 5px;
  line-height: 20px;
`;

export const ProviderLogo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
  margin: 10px auto;
`;

export const DetailsButton = styled.TouchableOpacity`
  flex-direction: "row";
  justify-content: "space-between";
  align-items: "center";
`;
