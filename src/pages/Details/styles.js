import { StyleSheet } from "react-native";
import styled from "styled-components/native";
import { LinearGradient } from "expo-linear-gradient";
import Constants from "expo-constants";

export const Container = styled(LinearGradient).attrs({
  colors: ["#F8F8FF", "#FFFAF0"],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  padding-top: ${Constants.statusBarHeight} + 20px;
`;

export const Logo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
`;
export const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 20px;
`;

export const GoBack = styled.TouchableOpacity``;

export const ProviderData = styled.View`
  flex: 1;
`;
export const Title = styled.Text`
  text-align: center;
  font-size: 32px;
  color: #fff;
  font-weight: bold;
  padding: 0 20px;
`;

export const ProviderBox = styled.View`
  padding: 20px;
  border-radius: 4px;
  background-color: #008037;
  margin-bottom: 15px;
`;
export const DescriptionBox = styled.View`
  padding: 20px;
  border-radius: 4px;
  background-color: #008037;
  margin-bottom: 15px;
`;
export const ContactBox = styled.View`
  padding: 20px;
  border-radius: 4px;
  background-color: #008037;
  margin-bottom: 15px;
`;

export const Content = styled.Text`
  font-size: 18px;
  color: #fff;
`;

export const Description = styled.Text.attrs({
  numberOfLines: 2,
})`
  color: #fff;
  margin-top: 5px;
  line-height: 20px;
`;

export const ProviderLogo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
  margin: 20px auto;
`;

export const DetailsButton = styled.TouchableOpacity`
  flex-direction: "row";
  justify-content: "space-between";
  align-items: "center";
`;
export const SocialLink = styled.TouchableOpacity`
  color: black;
  border-radius: 8;
  height: 50px;
  width: 50px;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
`;

export const Buttons = styled.View`
  margin-top: 20px;
  flex-direction: row;
  justify-content: "space-between";
`;
