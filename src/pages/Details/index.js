import React from "react";
import { Feather } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";
import * as MailComposer from "expo-mail-composer";
import { Image, Linking, ScrollView } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import logoImg from "../../assets/icon.png";
import styles, {
  Container,
  Header,
  Logo,
  GoBack,
  ProviderBox,
  ProviderLogo,
  DescriptionBox,
  ContactBox,
  Title,
  Content,
  ProviderData,
  SocialLink,
  Buttons,
} from "./styles";

export default function Details() {
  const navigation = useNavigation();
  const route = useRoute();
  const provider = route.params.provider;
  const message = `Olá, ${provider.name}. Vi seu anúncio no app Cartãozinho e gostaria de entrar em contato com vocês.`;
  function navigateBack() {
    navigation.goBack();
  }
  function sendMail() {
    MailComposer.composeAsync({
      subject: "Teste",
      recipients: [provider.email],
      body: message,
    });
  }
  function sendWhatsapp() {
    Linking.openURL(
      `whatsapp://send?phone=55${provider.phone}&text=${message}`
    );
  }
  function openTwitter() {
    Linking.openURL(`twitter://user?screen_name=${provider.twitter}`);
  }
  function openInstagram() {
    Linking.openURL(`instagram://user?username=${provider.instagram}`);
  }
  function openWebsite() {
    Linking.openURL(provider.website);
  }
  return (
    <Container>
      <Header>
        <Logo source={logoImg} />
        <GoBack onPress={navigateBack}>
          <Feather name="arrow-left" size={28} color="#008037" />
        </GoBack>
      </Header>
      <ProviderData>
        <ScrollView
          vertical
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ paddingHorizontal: 40 }}
        >
          <ProviderBox>
            <ProviderLogo
              source={`http://192.168.0.105:3001/files/${provider.avatar}`}
            />
            <Content>{provider.name}</Content>
            <Content>Estado: {provider.uf}</Content>
            <Content>Cidade: {provider.city}</Content>
            <Content>Bairro: {provider.neighborhood}</Content>
            <Content>Área de atuação: {provider.specialty}</Content>
            <Content>Tipo: {provider.type}</Content>
          </ProviderBox>
          {provider.description && (
            <DescriptionBox>
              <Title>Descrição</Title>
              <Content>{provider.description}</Content>
            </DescriptionBox>
          )}

          <ContactBox>
            <Title>Contato</Title>
            <Buttons>
              {provider.phone && (
                <SocialLink onPress={sendWhatsapp}>
                  <FontAwesome
                    name="whatsapp"
                    style={{
                      color: "#FFF",
                      fontSize: 30,
                      fontWeight: "bold",
                    }}
                  />
                </SocialLink>
              )}
              {provider.twitter && (
                <SocialLink onPress={openTwitter}>
                  <FontAwesome
                    name="twitter"
                    style={{
                      color: "#FFF",
                      fontSize: 30,
                      fontWeight: "bold",
                    }}
                  />
                </SocialLink>
              )}
              {provider.instagram && (
                <SocialLink onPress={openInstagram}>
                  <FontAwesome
                    name="instagram"
                    style={{
                      color: "#FFF",
                      fontSize: 30,
                      fontWeight: "bold",
                    }}
                  />
                </SocialLink>
              )}

              {provider.website && (
                <SocialLink onPress={openWebsite}>
                  <MaterialIcons
                    name="language"
                    style={{
                      color: "#FFF",
                      fontSize: 30,
                      fontWeight: "bold",
                    }}
                  />
                </SocialLink>
              )}

              {provider.email && (
                <SocialLink onPress={sendMail}>
                  <MaterialIcons
                    name="email"
                    style={{
                      color: "#FFF",
                      fontSize: 30,
                      fontWeight: "bold",
                    }}
                  />
                </SocialLink>
              )}
            </Buttons>
          </ContactBox>
        </ScrollView>
      </ProviderData>
    </Container>
  );
}
