import * as React from "react";
import logoImg from "../../assets/icon.png";

import { Container, Header, Logo, Content } from "./styles";

export default function Info() {
  return (
    <Container>
      <Header>
        <Logo source={logoImg} />
      </Header>
      <Content>
        Este app é um agregador de prestadores de serviços e pequenas empresas
        gratuito. Foi desenvolvido com o intuito de divulgar seu trabalho de
        forma fácil, rápida e sem agredir o meio ambiente. Por ser gratuito, o
        app possui anúncios, para que o desenvolvedor que vos fala também possa
        faturar.
      </Content>
    </Container>
  );
}
