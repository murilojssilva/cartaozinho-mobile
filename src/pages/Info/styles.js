import { StyleSheet } from "react-native";
import styled from "styled-components/native";
import { LinearGradient } from "expo-linear-gradient";
import Constants from "expo-constants";

export const Container = styled(LinearGradient).attrs({
  colors: ["#F8F8FF", "#FFFAF0"],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  padding-top: ${Constants.statusBarHeight} + 20px;
`;

export const Logo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
`;
export const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 20px;
`;

export const Content = styled.Text`
  font-size: 18px;
  color: #008037;
  font-weight: bold;
  margin: auto;
  place-items: center;
  width: 80%;
  text-align: center;
`;
