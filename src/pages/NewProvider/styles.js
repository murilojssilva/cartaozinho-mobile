import styled from "styled-components/native";
import { LinearGradient } from "expo-linear-gradient";
import Constants from "expo-constants";

export const Container = styled(LinearGradient).attrs({
  colors: ["#F8F8FF", "#FFFAF0"],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  padding-top: ${Constants.statusBarHeight} + 20px;
`;

export const Logo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
`;
export const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 20px;
`;
export const ProviderList = styled.View`
  flex: 1;
`;

export const Avatar = styled.Image`
  margin: 20px auto;
  width: 200px;
  height: 200px;
  border-radius: 25px;
`;
export const Input = styled.TextInput.attrs({
  placeholderTextColor: "#008037",
})`
  flex: 1;
  padding: 15px 15px;
  margin: 5px auto;
  border-radius: 4px;
  width: 90%;
  font-size: 16px;
  color: #333;
  background: #fff;
  border: 2px solid ${(props) => (props.error ? "#FF7272" : "#008037")};
`;

export const ProviderTitle = styled.Text`
  font-size: 32px;
  color: #008037;
  font-weight: bold;
  padding: 0 20px;
`;
export const Form = styled.View`
  flex-direction: row;
  margin: 10px 0;
  padding: 0 20px;
`;

export const List = styled.FlatList.attrs({
  contentContainerStyle: { paddingHorizontal: 20 },
  showsVerticalScrollIndicator: false,
})`
  margin-top: 20px;
`;
export const ProviderBox = styled.View`
  padding: 20px;
  border-radius: 4px;
  background-color: #008037;
  margin-bottom: 15px;
`;

export const Info = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: #008037;
`;

export const Description = styled.Text.attrs({
  numberOfLines: 2,
})`
  color: #fff;
  text-align: center;
  margin-top: 5px;
  line-height: 20px;
`;

export const Select = styled.Picker`
  height: 40px;
  background-color: "#fff";
  border-color: "#fff";
  border-radius: 10px;
  border-width: 0px;
  margin: 10px;
  padding: 10px;
`;

export const SelectView = styled.View`
  border-width: 0;
  background-color: #fff;
  color: #000;
`;

export const ProviderLogo = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 25px;
  margin: 10px auto;
`;

export const DetailsButton = styled.TouchableOpacity`
  flex-direction: "row";
  justify-content: "space-between";
  align-items: "center";
`;

export const Submit = styled.TouchableOpacity`
  color: black;
  border-radius: 8;
  height: 50px;
  width: 200px;
  justify-content: center;
  align-items: center;
  margin: 10px auto;
  background-color: #008037;
`;
export const Upload = styled.TouchableOpacity`
  color: black;
  border-radius: 8;

  background-color: blue;
  height: 50px;
  width: 100%;
  justify-content: center;
  align-items: center;
  margin: 10px auto;
`;

export const TextButton = styled.Text`
  margin-top: 20px;
  flex-direction: row;
  margin: auto;
  justify-content: space-between;
  font-size: 20px;
  font-weight: bold;
  color: #fff;
`;
