import React, { useState, useRef, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";

import defaultImage from "../../assets/default_image.png";

import ibge from "../../services/ibge";
import { Picker, ScrollView, Alert } from "react-native";
import api from "../../services/api";
import logoImg from "../../assets/icon.png";
import styles, {
  Container,
  Header,
  Input,
  Logo,
  ProviderTitle,
  AvatarView,
  Avatar,
  Submit,
  TextButton,
  Upload,
  Info,
  Select,
  ProviderList,
  SelectView,
} from "./styles";

export default function NewProvider() {
  const nameRef = useRef();
  const emailRef = useRef();
  const phoneRef = useRef();
  const stateRef = useRef();
  const twitterRef = useRef();
  const instagramRef = useRef();
  const websiteRef = useRef();
  const descriptionRef = useRef();
  const specialtyRef = useRef();
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [avatar, setAvatar] = useState(null);
  const [specialty, setSpecialty] = useState("");
  const [type, setType] = useState("");
  const [twitter, setTwitter] = useState("");
  const [instagram, setInstagram] = useState("");
  const [website, setWebsite] = useState("");
  const [pickerVisible, setPickerVisible] = useState(false);
  const [uf, setUf] = useState([]);
  const [city, setCity] = useState([]);
  const [loading, setLoading] = useState(false);
  const [ufSelect, setUfSelect] = useState("");
  const [citySelect, setCitySelect] = useState("");
  const [phone, setPhone] = useState("");
  const [description, setDescription] = useState("");

  async function loadUf() {
    const response = await ibge.get("estados");
    setUf(response.data);
    setLoading(false);
  }

  async function imagePickerCall() {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

      if (status !== "granted") {
        alert("Nós precisamos dessa permissão.");
        return;
      }
    }

    const data = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
      base64: true,
    });
    console.log(data);
    if (data.cancelled) {
      return;
    }

    if (!data.uri) {
      return;
    }
    setAvatar(data);
  }

  async function handleSubmit() {
    if (
      !avatar ||
      !name ||
      !email ||
      !description ||
      !type ||
      !ufSelect ||
      !citySelect ||
      !phone ||
      !specialty
    ) {
      Alert.alert(
        "Erro ao enviar dados",
        "Confira se você enviou todos os dados e tente novamente."
      );
      return;
    }
    const localUri = avatar.uri;
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };
    const avatarName = localUri.split("/").pop();
    const data = new FormData();
    setLoading(true);
    try {
      data.append("avatar", avatar);
      data.append("avatarUri", avatar.uri);
      data.append("avatarType", avatar.type);
      data.append("avatarName", avatarName);
      data.append("name", name);
      data.append("email", email);
      data.append("phone", phone);
      data.append("website", website);
      data.append("twitter", twitter);
      data.append("instagram", instagram);
      data.append("specialty", specialty);
      data.append("description", description);
      data.append("type", type);
      data.append("ufSelect", ufSelect);
      data.append("citySelect", citySelect);
      console.log(data);
      navigation.goBack();
      await api.post("/providers", data, config);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  }
  async function loadCity() {
    if (ufSelect !== "") {
      const response = await ibge.get(`estados/${ufSelect}/municipios`);
      setCity(response.data);
      setPickerVisible(true);
    }
  }

  useEffect(() => {
    loadUf();
  }, []);
  useEffect(() => {
    loadCity();
  }, [ufSelect]);
  return (
    <Container>
      <Header>
        <Logo source={logoImg} />
      </Header>
      <ProviderTitle>
        Cadastrar prestador de serviço/estabelecimento
      </ProviderTitle>
      <ProviderList>
        <ScrollView
          vertical
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ paddingHorizontal: 20 }}
        >
          {avatar ? (
            <Avatar source={{ uri: avatar.uri }} />
          ) : (
            <Avatar source={defaultImage} />
          )}
          <Input
            placeholder="* Nome do(a) prestador(a)/empresa"
            onChange={(e) => setName(e.target.value)}
            blurOnSubmit={true}
            value={name}
            returnKeyType="next"
            ref={nameRef}
            onSubmitEditing={() => emailRef.current.focus()}
          />
          <Input
            placeholder="* E-mail"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            blurOnSubmit={true}
            keyboardType="email-address"
            returnKeyType="next"
            ref={emailRef}
            onSubmitEditing={() => phoneRef.current.focus()}
          />
          <Input
            placeholder="* Número do whatsapp"
            onChange={(e) => setPhone(e.target.value)}
            dataDetectorTypes="phoneNumber"
            keyboardType="phone-pad"
            value={phone}
            blurOnSubmit={true}
            returnKeyType="next"
            ref={phoneRef}
            onSubmitEditing={() => websiteRef.current.focus()}
          />
          <Input
            placeholder="Website"
            blurOnSubmit={true}
            onChange={(e) => setWebsite(e.target.value)}
            value={website}
            returnKeyType="next"
            ref={websiteRef}
            onSubmitEditing={() => twitterRef.current.focus()}
          />
          <Input
            placeholder="Usuário no Twitter"
            onChange={(e) => setTwitter(e.target.value)}
            value={twitter}
            blurOnSubmit={true}
            returnKeyType="next"
            ref={twitterRef}
            onSubmitEditing={() => instagramRef.current.focus()}
          />
          <Input
            placeholder="Usuário no Instagram"
            onChange={(e) => setInstagram(e.target.value)}
            value={instagram}
            returnKeyType="next"
            blurOnSubmit={true}
            ref={instagramRef}
            onSubmitEditing={() => stateRef.current.focus()}
          />
          <SelectView>
            <Select
              blurOnSubmit={true}
              selectedValue={ufSelect}
              onValueChange={(itemValue) => setUfSelect(itemValue)}
            >
              <Picker.Item
                enabled={false}
                label="* Selecione o Estado"
                value={null}
                color="#00000090"
              />
              {uf
                .sort((a, b) => (a.nome > b.nome ? 1 : -1))
                .map((item, index) => {
                  return (
                    <Picker.Item
                      label={item.nome}
                      value={item.sigla}
                      key={item.id}
                    />
                  );
                })}
            </Select>
          </SelectView>
          <SelectView>
            <Select
              blurOnSubmit={true}
              selectedValue={citySelect}
              enabled={pickerVisible}
              onValueChange={(itemValue) => setCitySelect(itemValue)}
            >
              <Picker.Item
                enabled={false}
                label="* Selecione a cidade"
                value={null}
                color="#00000090"
              />
              {city
                .sort((a, b) => (a.nome > b.nome ? 1 : -1))
                .map((item, index) => {
                  return (
                    <Picker.Item
                      label={item.nome}
                      value={item.nome}
                      key={item.id}
                    />
                  );
                })}
            </Select>
          </SelectView>
          <SelectView>
            <Select
              blurOnSubmit={true}
              selectedValue={type}
              onValueChange={(itemValue) => setType(itemValue)}
            >
              <Picker.Item
                enabled={false}
                label="* Selecione o seu tipo de pessoa"
                value={null}
              />
              <Picker.Item label="Pessoa física" value="Pessoa física" />
              <Picker.Item label="Pessoa jurídica" value="Pessoa jurídica" />
              <Picker.Item label="Estabelecimento" value="Estabelecimento" />
            </Select>
          </SelectView>
          <Input
            placeholder="* Área de atuação"
            onChange={(e) => setSpecialty(e.target.value)}
            value={specialty}
            returnKeyType="next"
            blurOnSubmit={true}
            ref={specialtyRef}
            onSubmitEditing={() => descriptionRef.current.focus()}
          />
          <Input
            placeholder="* Descrição"
            onChange={(e) => setDescription(e.target.value)}
            multiline
            numberOfLines={10}
            blurOnSubmit={true}
            value={description}
            ref={descriptionRef}
            returnKeyType="done"
          />
          <Upload onPress={imagePickerCall} blurOnSubmit={true}>
            <TextButton>Selecione a logomarca/foto</TextButton>
          </Upload>

          <Submit onPress={handleSubmit}>
            <TextButton>{loading ? "Enviando..." : "Cadastrar"}</TextButton>
          </Submit>
        </ScrollView>
      </ProviderList>
      <Info>* - Itens obrigatórios.</Info>
    </Container>
  );
}
