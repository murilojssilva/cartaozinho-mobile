import React from "react";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Services from "./pages/Services";
import Details from "./pages/Details";
import Info from "./pages/Info";
import NewProvider from "./pages/NewProvider";
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Tabs() {
  return (
    <Tab.Navigator
      initialRouteName="Serviços"
      tabBarOptions={{
        activeTintColor: "#008037",
      }}
    >
      <Tab.Screen
        name="Serviços"
        component={Services}
        options={{
          tabBarLabel: "Serviços",
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="work" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Cadastrar prestador/empresa"
        component={NewProvider}
        options={{
          tabBarLabel: "Cadastrar prestador/empresa",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="plus-circle"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Informações"
        component={Info}
        options={{
          tabBarLabel: "Informações",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="information"
              color={color}
              size={size}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
export default function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Services" component={Tabs} />
        <Stack.Screen name="Details" component={Details} />
        <Stack.Screen name="Info" component={Info} />
        <Stack.Screen name="Serviços" component={Services} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
