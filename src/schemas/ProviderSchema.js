export default class ProviderSchema {
  static schema = {
    name: "Provider",
    primaryKey: "id",
    properties: {
      id: { type: "string", indexed: true },
      name: "string",
      type: "string",
    },
  };
}
