import axios from "axios";

const api = axios.create({
  baseURL: "https://cartaozinho-backend.herokuapp.com",
});

export default api;
