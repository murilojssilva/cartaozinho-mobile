import Realm from "realm";

import ProviderSchema from "../schemas/ProviderSchema";

export default function getRealm() {
  return Realm.open({
    schema: [ProviderSchema],
  });
}
