import { takeLatest, call, put, all } from "redux-saga/effects";
import api from "../../../services/api";
import { Alert } from "react-native";

import { updateProfileSuccess, updateProfileFailure } from "./actions";

export function* updateProfile({ payload }) {
  try {
    const { name, email } = payload.data;
    const profile = Object.assign({ name, email });
    const response = yield call(api.put, "users", profile);
    //toast.success("Perfil atualizado com sucesso.");
    Alert.alert("Sucesso", "Perfil atualizado com sucesso.");
    yield put(updateProfileSuccess(response.data));
  } catch (err) {
    //toast.error("Erro ao atualizar perfil");
    Alert.alert(
      "Falha ao atualizar perfil.",
      "Erro ao realizar a atualização de perfil. Tente novamente."
    );
    yield put(updateProfileFailure());
  }
}

export default all([takeLatest("@user/UPDATE_PROFILE_REQUEST", updateProfile)]);
